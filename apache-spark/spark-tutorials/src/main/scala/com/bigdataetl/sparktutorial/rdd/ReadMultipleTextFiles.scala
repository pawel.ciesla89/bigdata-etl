package com.bigdataetl.sparktutorial.rdd

import org.apache.spark.sql.SparkSession

object ReadMultipleTextFiles extends App {

  val spark: SparkSession = SparkSession
    .builder()
    .master("local[*]")
    .appName("BigData-ETL.com")
    .getOrCreate()

  println("Spark Read All Text Files From A Directory Into A Single RDD")
  val dataAsRDD = spark.sparkContext.textFile("src/main/resources/ReadMultipleTextFiles/txt/subDir1/*")
  dataAsRDD.foreach(data => println(data))


  println("Read the same data using the wholeTextFiles() method. This methods returns the RDD[Tuple2] object")
  val dataAsRDDByWhole = spark.sparkContext.wholeTextFiles("src/main/resources/ReadMultipleTextFiles/txt/subDir1/*")
  dataAsRDDByWhole.foreach(f => {
    println(s"In file:[${f._1}], found data:[${f._2}]")
  })

  println("Spark Read Several Text Files Into A Single RDD")
  val dataAsRDD2 = spark.sparkContext.textFile("src/main/resources/ReadMultipleTextFiles/txt/subDir1/file_001.txt,src/main/resources/ReadMultipleTextFiles/txt/subDir1/file_002.txt")
  dataAsRDD2.foreach(data => println(data))

  println("Spark Read All Files Which Match The Pattern To Single RDD")
  val dataAsRDD3 = spark.sparkContext.textFile("src/main/resources/ReadMultipleTextFiles/txt/subDir1/file_*.txt")
  dataAsRDD3.foreach(data => println(data))


  println("Spark Read Files From Multiple Directories")
  val dataAsRDD4 = spark.sparkContext.textFile("src/main/resources/ReadMultipleTextFiles/txt/subDir1/*,src/main/resources/ReadMultipleTextFiles/txt/subDir2/*")
  dataAsRDD4.foreach(data => println(data))
}
