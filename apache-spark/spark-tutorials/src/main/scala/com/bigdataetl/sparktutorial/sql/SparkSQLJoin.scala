package com.bigdataetl.sparktutorial.sql

import org.apache.spark.sql.SparkSession

object SparkSQLJoin extends App {

  val spark: SparkSession = SparkSession
    .builder()
    .master("local[*]")
    .appName("BigData-ETL.com")
    .getOrCreate()

  import spark.implicits._

  val customers = Seq(
    (1, "John Smith"),
    (2, "Jane Doe"),
    (3, "Bob Johnson")
  ).toDF("id", "name")

  val orders = Seq(
    (1, 100),
    (2, 200),
    (3, 300)
  ).toDF("customer_id", "amount")

  customers
    .join(orders, customers("id") === orders("customer_id"), "inner")
    .show()

  val departments = Seq(
    (1, "IT"),
    (2, "HR"),
    (3, "Marketing")
  ).toDF("id", "name")

  val employees = Seq(
    (1, "John Smith", 1),
    (2, "Jane Doe", 2),
    (3, "Bob Johnson", 1)
  ).toDF("id", "name", "department_id")

  employees
    .join(departments, employees("department_id") === departments("id"), "left")
    .show()

  val products = Seq(
    (1, "Product1", "Category1"),
    (2, "Product2", "Category2"),
    (3, "Product3", "Category3")
  ).toDF("id", "name", "category")

  val sales = Seq(
    (1, 100, 1),
    (2, 200, 2),
    (3, 300, 1)
  ).toDF("id", "amount", "product_id")

  products.join(sales, products("id") === sales("product_id"), "right").show()

  val stores = Seq(
    (1, "Store1"),
    (2, "Store2"),
    (3, "Store3")
  ).toDF("id", "name")

  val inventory = Seq(
    (1, "Product1", 10),
    (2, "Product2", 20),
    (3, "Product3", 30),
    (4, "Product4", 40)
  ).toDF("product_id", "name", "quantity")

  stores
    .join(inventory, stores("id") === inventory("product_id"), "full")
    .show()

  val fruits =
    Seq(("Apple"), ("Banana"), ("Grapes"), ("Strawberries")).toDF("name")
  val color = Seq(("Red"), ("Yellow"), ("Green"), ("Orange")).toDF("color")
  fruits.crossJoin(color).show()

  val books = Seq(
    (1, "Book1", "Author1"),
    (2, "Book2", "Author2"),
    (3, "Book3", "Author3"),
    (4, "Book4", "Author4")
  ).toDF("id", "name", "author")

  val sales2 = Seq(
    (1, 100),
    (3, 200)
  ).toDF("book_id", "sales")

  books.join(sales2, books("id") === sales2("book_id"), "semi").show()

  val countries = Seq(
    (1, "US"),
    (2, "UK"),
    (3, "CN")
  ).toDF("id", "name")

  val languages = Seq(
    (1, "English"),
    (2, "Chinese")
  ).toDF("country_id", "language")

  countries.join(languages, countries("id") === languages("country_id"), "anti")

  val students = Seq(
    (1, "John Smith"),
    (2, "Jane Doe"),
    (3, "Bob Johnson")
  ).toDF("id", "name")

  val grades = Seq(
    (1, "A"),
    (2, "B")
  ).toDF("student_id", "grade")

  students
    .join(grades, students("id") === grades("student_id"), "left_anti")
    .show()

}
