package com.bigdataetl.sparktutorial.sql

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, lit}
import org.apache.spark.sql.types.DoubleType

object SparkWithColumn extends App {

  val spark: SparkSession = SparkSession
    .builder()
    .master("local[*]")
    .appName("BigData-ETL.com")
    .getOrCreate()

  import spark.implicits._

  val carsData = Seq(
    ("Ford Torino", 140, 3449, "US"),
    ("Chevrolet Monte Carlo", 150, 3761, "US"),
    ("BMW 2002", 113, 2234, "Europe")
  )
  val columns = Seq("car", "horsepower", "weight", "origin")
  val carsDf = carsData.toDF(columns: _*)

  println("Replace Column Value In DataFrame")
  carsDf
    .withColumn("weight", lit(2000))
    .show()

  println("Add New Column To DataFrame")
  carsDf
    .withColumn("city", lit("unknown"))
    .withColumn("continent", lit("unknown"))

  println("Acquire New Column Based On Existing Column In DataFrame")
  carsDf
    .withColumn("kilowatt_power", col("horsepower") * lit(0.7457))
    .show()

  println("Change Column Data Type In DataFrame")
  carsDf
    .withColumn("horsepower", col("horsepower").cast(DoubleType))
    .show()

  println("Spark Rename Column In DataFrame")
  carsDf
    .withColumnRenamed("car", "car_name")
    .printSchema()

  println("Update, Replace or Add Multiple Columns In DataFrame")
  val colsMap = Map(
    "kilowatt_power" -> col("horsepower") * lit(0.7457),
    "double_kilowatt_power" -> col("horsepower") * lit(0.7457 * 2)
  )

  carsDf
    .withColumns(colsMap)
    .show()

  carsDf.createOrReplaceTempView("cars")
  spark
    .sql(
      "SELECT horsepower * 0.7457 as kilowatt_power, horsepower * 0.7457 * 2 as double_kilowatt_power FROM cars"
    )
    .show()

  println("Spark Delete Column From DataFrame")
  carsDf
    .drop("car")
    .show()
}
