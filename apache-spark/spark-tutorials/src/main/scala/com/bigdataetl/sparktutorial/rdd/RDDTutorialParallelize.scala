package com.bigdataetl.sparktutorial.rdd

import org.apache.spark.sql.SparkSession

object RDDTutorialParallelize extends App {

  val spark: SparkSession = SparkSession
    .builder()
    .master("local[*]")
    .appName("BigData-ETL.com")
    .getOrCreate()

  val dataAsRDD = spark.sparkContext.parallelize(Array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))

  println(s"Is RDD empty?: ${dataAsRDD.isEmpty}")
  println(s"First element of RDD: ${dataAsRDD.first}")
  println(s"Number of RDD Partitions: ${dataAsRDD.getNumPartitions}")

  dataAsRDD.collect().foreach(element => println(s"Element: $element"))

}
