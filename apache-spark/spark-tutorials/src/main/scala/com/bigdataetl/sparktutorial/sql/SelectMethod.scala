package com.bigdataetl.sparktutorial.sql

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col

object SelectMethod extends App {

  val spark: SparkSession = SparkSession
    .builder()
    .master("local[*]")
    .appName("BigData-ETL.com")
    .getOrCreate()

  import spark.implicits._

  val carsData = Seq(
    ("Ford Torino",140,3449,"US"),
    ("Chevrolet Monte Carlo",150,3761,"US"),
    ("BMW 2002",113,2234,"Europe")
  )
  val columns = Seq("car","horsepower","weight","origin")
  val carsDf = carsData.toDF(columns:_*)
  carsDf.show(false)

  println("Spark Select Single Column")
  carsDf.select("car").show(false)
  carsDf.select(col("car")).show(false)

  println("Spark Select Multiple Columns")
  carsDf.select("car", "origin", "horsepower").show(false)

  val subColumns = Seq("origin", "horsepower")
  carsDf.select("car", subColumns: _*).show(false)
  carsDf.select(subColumns.map(c => col(c)): _*).show(false)

  println("Spark Select By Position Index In DataFrame")
  carsDf.select(carsDf.columns(2)).show()

  println("Spark Select Columns From Sequence")
  val colList = List("origin", "horsepower")
  carsDf.select(colList.map(c => col(c)): _*).show(false)

  val colSeq = Seq("origin", "horsepower")
  carsDf.select(colSeq.map(c => col(c)): _*).show(false)

  val colArray = Array("origin", "horsepower")
  carsDf.select(colArray.map(c => col(c)): _*).show(false)

  println("Spark Select N Columns From DataFrame")
  val myDfColumns: Array[String] = carsDf.columns
  println(myDfColumns.mkString(","))

  carsDf.select(carsDf.columns.slice(0,2).map(c => col(c)): _*).show()
  carsDf.select(carsDf.columns.slice(1,3).map(c => col(c)): _*).show()

  println("Spark Select Columns By Regular Expression")
  carsDf.select(carsDf.colRegex("`^horse.*`")).show()

  println("Spark Select Columns Which Contains")
  carsDf.select(carsDf.columns.filter(_.contains("r")).map(c => col(c)): _*).show()

  println("Spark Select Columns Which Starts With")
  carsDf.select(carsDf.columns.filter(_.startsWith("c")).map(c => col(c)): _*).show()

  println("Spark Select Columns Which Ends With")
  carsDf.select(carsDf.columns.filter(_.endsWith("t")).map(c => col(c)): _*).show()

  println("Spark Select Column From DataFrame Using SelectExpr Function")
  carsDf.selectExpr("car as car_name").show()
}
