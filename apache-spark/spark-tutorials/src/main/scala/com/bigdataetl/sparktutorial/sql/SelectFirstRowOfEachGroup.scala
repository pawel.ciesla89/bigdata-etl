package com.bigdataetl.sparktutorial.sql

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{col, row_number}

object SelectFirstRowOfEachGroup extends App {

  val spark: SparkSession = SparkSession
    .builder()
    .master("local[*]")
    .appName("BigData-ETL.com")
    .getOrCreate()

  val carsData = spark.read
    .option("delimiter", ";")
    .option("header", "true")
    .option("inferSchema", true)
    .csv("src/main/resources/cars.csv")


  /**
  * Case #1 - Select The Car Which Has The Highest Horsepower By Origin
  * */
  val carWithTheMostHorsepowerPerOrigin = carsData.select("car", "horsepower", "origin")
    .withColumn(
      "row",
      row_number().over(
        Window.partitionBy("origin").orderBy(col("horsepower").desc)
      )
    )
    .where(col("row") === 1)
    .drop("row")

  carWithTheMostHorsepowerPerOrigin.show(false)

  /**
   * Case #2 - Select The Car Which Has The Highest Amount of Cylinders With The Lowest Weight By Origin
   * */
  val carWithTheMostCylindersAndTheLowestWeightByOrigin = carsData.select("car", "cylinders", "weight" , "origin")
    .withColumn(
      "row",
      row_number().over(
        Window.partitionBy("origin").orderBy(col("cylinders").desc, col("cylinders").asc)
      )
    )
    .where(col("row") === 1)
    .drop("row")

  carWithTheMostCylindersAndTheLowestWeightByOrigin.show(false)


  /**
   * Case #3 - Select The Car Which Has The Highest Horsepower By Origin And Cylinders Amount
   * */
  val carWithTheMostHorsepowerByOriginAndCylinders = carsData.select("car", "horsepower", "origin", "cylinders")
    .withColumn(
      "row",
      row_number().over(
        Window.partitionBy("origin", "cylinders").orderBy(col("horsepower").desc)
      )
    )
    .where(col("row") === 1)
    .drop("row")

  carWithTheMostHorsepowerByOriginAndCylinders.show(false)

}
