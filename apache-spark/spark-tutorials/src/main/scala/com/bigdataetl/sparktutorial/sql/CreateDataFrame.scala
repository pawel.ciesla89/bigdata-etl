package com.bigdataetl.sparktutorial.sql

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}

object CreateDataFrame extends App {

  val spark: SparkSession = SparkSession
    .builder()
    .master("local[*]")
    .appName("BigData-ETL.com")
    .getOrCreate()

  val data = Seq(("Paul", 30), ("Tom", 34), ("George", 55))
    val dataAsRDD = spark.sparkContext.parallelize(data)
//  val dataAsRDD: RDD[(String, Int)] = spark.sparkContext.parallelize(data)

  println("Create DataFrame Using toDF() Method")
  import spark.implicits._
  val toDfResults = dataAsRDD.toDF()
  toDfResults.show()
  toDfResults.printSchema()

//  val toDfResultsWithColNames = dataAsRDD.toDF("name", "age")
  val columns = Seq("name", "age")
  val toDfResultsWithColNames = dataAsRDD.toDF(columns: _*)
  toDfResultsWithColNames.show()
  toDfResultsWithColNames.printSchema()


  println("Create DataFrame Using createDataFrame() Method From SparkSession")
  val createDataFrameResults = spark.createDataFrame(dataAsRDD).toDF(columns: _*)
  createDataFrameResults.printSchema()

  println("Create DataFrame With Schema")
  val schema = StructType(Array(
    StructField("name", StringType, false),
    StructField("age", IntegerType, true)
  ))
  val rowRDD: RDD[Row] = dataAsRDD.map(element => Row(element._1, element._2))
  val createDataFrameWithSchema = spark.createDataFrame(rowRDD, schema)
  createDataFrameWithSchema.printSchema()

  println("Recreate DataFrame to break DAG lineage")
  val recreateDataFrame = spark.createDataFrame(createDataFrameWithSchema.rdd, schema)
  recreateDataFrame.printSchema()

  val dfFromCSV =  spark.read.csv("path/to/dataAs.csv")
  val dfFromJson =  spark.read.json("path/to/dataAs.json")
  val dsFromText =  spark.read.textFile("path/to/dataAs.txt")


}
