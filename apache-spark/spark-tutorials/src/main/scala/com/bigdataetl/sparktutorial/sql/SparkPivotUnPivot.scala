package com.bigdataetl.sparktutorial.sql

import org.apache.spark.sql.{SparkSession, functions}

object SparkPivotUnPivot extends App {

  val spark: SparkSession = SparkSession
    .builder()
    .master("local[*]")
    .appName("BigData-ETL.com")
    .getOrCreate()

  import spark.implicits._


  val dfForPivot = Seq(
    (1, "a", "x", 5, 10),
    (2, "b", "x", 3, 20),
    (3, "c", "y", 4, 15)
  ).toDF("id", "name", "category", "value1", "value2")

  val pivotDf = dfForPivot.groupBy("id", "name").pivot("category", Seq("x", "y"))
    .agg(functions.sum("value1"), functions.sum("value2"))

  pivotDf.show()

  val dfForUnpivot = Seq(
    (1, "a", 5, 10, 15),
    (2, "b", 3, 20, 25),
    (3, "c", 4, 15, 35)
  ).toDF("id", "name", "value1", "value2", "value3")

  val unpivotDf = dfForUnpivot.selectExpr("id", "name", "stack(3, 'value1', value1, 'value2', value2, 'value3', value3) as (attribute, value)")

  unpivotDf.show()
}
