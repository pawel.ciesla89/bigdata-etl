package com.bigdataetl.sparktutorial.sql

import org.apache.spark.sql.functions.{array_contains, col, lit}
import org.apache.spark.sql.types.{ArrayType, StringType, StructType}
import org.apache.spark.sql.{Row, SparkSession}

object SparkFilterAndWhere extends App {

  val spark: SparkSession = SparkSession
    .builder()
    .master("local[*]")
    .appName("BigData-ETL.com")
    .getOrCreate()

  import spark.implicits._

  val carsData = Seq(
    ("Ford Torino", 140, 3449, "US"),
    ("Chevrolet Monte Carlo", 150, 3761, "US"),
    ("BMW 2002", 113, 2234, "Europe"),
    ("Volkswagen Model 111", 97, 1834, "Europe"),
    ("Datsun 510 (sw)", 97, 2288, "Japan")
  )
  val columns = Seq("car", "horsepower", "weight", "origin")
  val carsDf = carsData.toDF(columns: _*)

  carsDf.printSchema()
  carsDf.show(false)

  println("Spark filter() DataFrame Using Column Condition")
  carsDf.filter(col("origin") === lit("US")).show()
  carsDf.filter($"origin" === lit("US")).show()
  carsDf.filter(carsDf("origin") === lit("US")).show()
  carsDf.filter('origin === lit("US")).show()

  carsDf.where(col("origin") === lit("US")).show()
  carsDf.where($"origin" === lit("US")).show()
  carsDf.where(carsDf("origin") === lit("US")).show()
  carsDf.where('origin === lit("US")).show()

  println("Spark Filter DataFrame Rows Using The Given SQL Expression")
  carsDf.filter("origin == 'US'").show()
  carsDf.where("origin == 'US'").show(false)

  println("Spark Filter DataFrame By Multiple Column Conditions")
  carsDf
    .filter(col("origin") === lit("US") && col("horsepower") >= lit(150))
    .show()

  carsDf
    .where(col("origin") === lit("US") && col("horsepower") >= lit(150))
    .show()

  println("Spark Filter Nested Struct In DataFrame Or DataSet")

  val arrayStructData = Seq(
    Row(Row("Denver", "New York"), List("5th Avenue", "7th Avenue"), "US"),
    Row(Row("Los Angeles", "San Francisco"), List("Street1", "Street2"), "US"),
    Row(
      Row("Paris", "Warsaw"),
      List("Al. Jerozolimskie", "S2", "Street3"),
      "EU"
    )
  )

  val arrayStructureSchema = new StructType()
    .add(
      "country",
      new StructType()
        .add("city1", StringType)
        .add("city2", StringType)
    )
    .add("streets", ArrayType(StringType))
    .add("country_code", StringType);

  val cityData = spark.createDataFrame(
    spark.sparkContext.parallelize(arrayStructData),
    arrayStructureSchema
  )
  cityData.printSchema()
  cityData.show(false)

  cityData
    .filter(cityData("country.city2") === "Warsaw")
    .show(false)

  println("Spark Filter Array Column In DataFrame Or DataSet")
  cityData
    .filter(array_contains(col("streets"), "7th Avenue"))
    .show(false)
}
