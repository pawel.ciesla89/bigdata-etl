<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://bigdata-etl.com">
    <img src="https://bigdata-etl.com/wp-content/uploads/2022/07/Youtube-Background_2.png" style="border-radius: 50px;" alt="Logo" width="200" height="100">
  </a>

<h3 align="center">Other Topics</h3>

  <p align="center">
    Check my main topics on Bigdata-ETL.com blog!
    <br />
    <a href="https://bigdata-etl.com"><strong>Explore the Blog »</strong></a>
    <br />
    <br />
    <a href="https://bigdata-etl.com/tag/apache-spark/">Spark</a>
    ·
    <a href="https://bigdata-etl.com/tag/apache-spark-tutorial/">Spark Tutorial</a>
    ·
    <a href="https://bigdata-etl.com/tag/big-data-en/">Big Data</a>
    ·
    <a href="https://bigdata-etl.com/big-data-articles/etl-articles/">Talend ETL</a>
  </p>
</div>